<?php
global $route;
use fw_Klipso\kernel\engine\middleware\Session;

$route->add('admin/','Admin.SistemaController.login');
$route->add('admin/cerrar-sesion','Admin.SistemaController.cerrarSesion');
$route->add(Session::loginRequired('admin/dashboard/'), 'Admin.SistemaController.dashboard');
$route->add(Session::loginRequired('admin/habitacion/listado'),'Admin.AdminController.listadoHabitaciones');
$route->add(Session::loginRequired('admin/habitacion'),'Admin.AdminController.habitacion');
$route->add(Session::loginRequired('admin/habitacion/editar/P{([0-9]+)}'), 'Admin.AdminController.editarHabitacion');
$route->add(Session::loginRequired('admin/habitacion/eliminar/P{([0-9]+)}'),'Admin.AdminController.eliminarHabitaciones');
$route->add(Session::loginRequired('admin/habitacion/subir-imagen'),'Admin.AdminController.subirImagen');
$route->add(Session::loginRequired('admin/habitacion/eliminar-imagen'),'Admin.AdminController.elimininarImagen');
$route->add(Session::loginRequired('admin/reservaciones'),'Admin.AdminController.listadoReservaciones');
$route->add(Session::loginRequired('admin/clientes'),'Admin.AdminController.listadoClientes'    );