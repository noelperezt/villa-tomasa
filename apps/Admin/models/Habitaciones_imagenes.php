<?php
    namespace apps\Admin\models;


    use fw_Klipso\kernel\classes\abstracts\aModels;
    use fw_Klipso\kernel\engine\dataBase\Constrainst;
    use fw_Klipso\kernel\engine\dataBase\DataType;
    use fw_Klipso\kernel\engine\dataBase\TypeFields;

    class Habitaciones_imagenes extends aModels {
        private $prefix_model = 'adm';

        public function __fields__(){
            $field = [
                'id_habitacion' => DataType::FieldInteger(),
                'ruta_archivo' => DataType::FieldString(100,true),
            ];
            return $field;
        }
        public function __foreignKey()
        {
            $fk = [
                'id_habitacion' => Constrainst::ForeignKey('Habitaciones', 'id_habitacion',
                    Constrainst::on_update(false),
                    Constrainst::on_delete(true)),
            ];
            return $fk;

        }

        public function __setPrimary()
        {
            // TODO: Implement __setPrimary() method.
        }

        public function __setUnique()
        {
            // TODO: Implement __setUnique() method.
        }

        public function __getPrefix()
        {
            return $this->prefix_model;
        }
    }