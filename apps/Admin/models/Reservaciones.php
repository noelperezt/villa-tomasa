<?php
namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

    class Reservaciones extends aModels {
        private $prefix_model = 'adm';
        public function __foreignKey()
        {
            $fk = [
                'id_cliente' => Constrainst::ForeignKey('Clientes', 'id_cliente',
                    Constrainst::on_update(false), Constrainst::on_delete(false)),
                'id_habitacion' => Constrainst::ForeignKey('Habitaciones', 'id_habitacion',
                    Constrainst::on_update(false), Constrainst::on_delete(false))
            ];
            return $fk;
        }

        public function __fields__()
        {
            $field = [
                'id_reservacion' => DataType::FieldAutoField(),
                'id_cliente' => DataType::FieldInteger(true),
                'id_habitacion' => DataType::FieldInteger(true),
                'cantidad_infante' => DataType::FieldInteger(true),
                'cantidad_adulto' => DataType::FieldInteger(true),
                'fecha_entrada' => DataType::FieldDateTime(true),
                'fecha_salida' => DataType::FieldDateTime(true),
                'estado' => DataType::FieldChar(true, 'S')

            ];
            return $field;
        }

        public function __setPrimary()
        {
            $pk = [
                'id_reservacion'
            ];
            return $pk;
        }

        public function __setUnique()
        {
            // TODO: Implement __setUnique() method.
        }

        public function __getPrefix()
        {
            return $this->prefix_model;
        }
    }