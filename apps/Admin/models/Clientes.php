<?php
namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Clientes extends aModels {
    private $prefix_model = 'adm';
    public function __foreignKey(){

    }

    public function __fields__()
    {
        $field = [
            'id_cliente' => DataType::FieldInteger(true),
            'nombre' => DataType::FieldString(80, true),
            'telefono_movil' => DataType::FieldInteger(true),
            'telefono_fijo' => DataType::FieldInteger(true),
            'correo' => DataType::FieldString(80,true),
            'estado' => DataType::FieldChar(true,'A'),

        ];
        return $field;
    }

    public function __setPrimary()
    {
        $pk = [
            'id_cliente'
        ];
        return $pk;
    }

    public function __setUnique()
    {
        // TODO: Implement __setUnique() method.
    }

    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}