<?php
    namespace apps\Admin\models;

    use fw_Klipso\kernel\classes\abstracts\aModels;
    use fw_Klipso\kernel\engine\dataBase\Constrainst;
    use fw_Klipso\kernel\engine\dataBase\DataType;
    use fw_Klipso\kernel\engine\dataBase\TypeFields;

    class Habitaciones extends aModels{
        private $prefix_model = 'adm';


        public function __foreignKey()
        {

        }

        public function __fields__()
        {
            $field = [
                'id_habitacion' => DataType::FieldAutoField(),
                'nombre_habitacion' => DataType::FieldString(80,true),
                'descripcion_habitacion' => DataType::FieldText(),
                'monto' => DataType::FieldFloat( 10, 2, false),
                'estado' => DataType::FieldChar(true,'A')
            ];
            return $field;
        }

        public function __setPrimary()
        {
            $pk = [
                'id_habitacion'
            ];
            return $pk;
        }

        public function __setUnique()
        {
            // TODO: Implement __setUnique() method.
        }

        public function __getPrefix()
        {
            return $this->prefix_model;
        }
    }