<?php
namespace apps\Admin\controllers;

use apps\Admin\models\Habitaciones;
use fw_Klipso\applications\images\UploadFile;
use fw_Klipso\applications\send_mail\SendMail;
use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;
use fw_Klipso\kernel\engine\middleware\Response;
use fw_Klipso\kernel\templates\TemplateList;


class AdminController extends aController {

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }
    public function listadoClientes(Request $request){
        $template = new TemplateList($request);
        $paramets = [
            'models' => 'adm_clientes',
            'fields' => 'id_cliente,nombre, telefono_movil, telefono_fijo, correo, estado',

        ];
        $template->setConf($paramets, $this->Model('Clientes'), 10);
        $context = $template->getPaginator('cliente_listado', ['titulo' => 'Listado de clientes']);
        $this->render('cliente_listado', $context);
    }
    public function listadoHabitaciones(Request $request){
        $template = new TemplateList($request);
        $paramets = [
            'models' => 'adm_habitaciones',
            'fields' => 'id_habitacion,nombre_habitacion, monto, estado',

        ];

        $template->setConf($paramets, $this->Model('Habitaciones'),10);
        $context = $template->getPaginator(['titulo' => 'Listado de habitaciones']);
        $this->render('habitaciones_listado', $context);
    }
    public function habitacion(Request $request){
        if($request->isPost()){
            $this->registrar($request);
        }
        $context =[
            'titulo' => 'Registrar Habitaciones',
            'action' => 'add',
            'cantidad_imagen' => 0
        ];
        $this->render('habitaciones', $context);
    }
    public function editarHabitacion(Request $request, $id_habitacion){
        $id_habitacion =  intval($id_habitacion);
        if(!is_int($id_habitacion))
            return;
        if($request->isPost()){
            $this->actualizar($request);
        }

        $habitaciones = $this->Model('Habitaciones');

        /* retorna los datos de la habitacion */
        $condicion = [
             $id_habitacion
        ];
        $field = 'select id_habitacion, nombre_habitacion, monto, estado, descripcion_habitacion 
                  from adm_habitaciones 
                  where id_habitacion = ?';
        $rs = $habitaciones->raw($field, $condicion);

        /* retorna los datos de las imagenes */
        $condicion = [
             intval($id_habitacion)
        ];
        $sql = 'select ruta_archivo from adm_habitaciones_imagenes where id_habitacion = ?';
        $rsImg = $habitaciones->raw($sql, $condicion);
        $Img = [];
        if(!isset($rsImg[0])){
            $Img[0] = $rsImg;
        }else{
            $Img  = $rsImg;
        }
        $context =[
            'titulo' => 'Actualizar datos de la  Habitación',
            'action' => 'edit',
            'habitacion' => $rs,
            'imagenes' => $Img,
            'cantidad_imagen' => count($rsImg)
        ];
        $this->render('habitaciones', $context);
    }
    private function actualizar($request){
        $habitaciones = new Habitaciones();
        $condicion = [
            'id_habitacion' => $request->_post('registro')
        ];
        $array_valor = [
            'nombre_habitacion' => $request->_post('nombre_habitacion'),
            'monto' => $request->_post('monto'),
            'estado' => $request->_post('estado'),
            'descripcion_habitacion' => $request->_post('descripcion_habitacion'),
            'estado' => $request->_post('estado'),
        ];
        $habitaciones->setUpdate($array_valor, $condicion);

        $this->guardarImagenesHabitacion($habitaciones->DB(), $request->_post('galeria_image'), $request->_post('registro'));

        Response::setMessage('habitación actualizada con éxito');
    }
    private function registrar(Request $request){
        $habitaciones = $this->Model('Habitaciones');
        $value = [
            'nombre_habitacion' => $request->_post('nombre_habitacion'),
            'monto' => $request->_post('monto'),
            'estado' => $request->_post('estado'),
            'descripcion_habitacion' => $request->_post('descripcion_habitacion'),
        ];

        $id_habitacion = $habitaciones->save($value);
        if(!count($id_habitacion) > 0){
            redirect('/admin/habitacion/','No se registro la habitacion');
            return;
        }
        $this->guardarImagenesHabitacion($request->_post('galeria_image'), $id_habitacion);

        redirect('/admin/habitacion/','habitación registrada con éxito');
        /* guarda las imagenes de la habitacion */

    }
    private function guardarImagenesHabitacion($array_imagenes, $id_habitacion){
        $model = $this->Model(('Habitaciones_imagenes'));
        $model->where(['id_habitacion{=}' => $id_habitacion])->delete();

        foreach ($array_imagenes as $value){
            $array = [
                'id_habitacion' => $id_habitacion,
                'ruta_archivo' => $value
            ];
            $model->save(
                $array
            );

        }
    }
    public function eliminarHabitaciones(Request $request, $id_habitacion){
        if($request->isPost())
            $this->eliminar($request);

        $habitacion = $this->Model('Habitaciones');
        $condicion = [$id_habitacion];

        $rs = $habitacion->raw('select id_habitacion, nombre_habitacion 
                                from adm_habitaciones 
                                where id_habitacion = ?', $condicion);
        $this->render('habitacion_eliminar', $rs);
    }
    private function eliminar(Request $request){
        $habitacion = $this->Model('Habitaciones');
        $condicion = [
            'id_habitacion{=}' => $request->_post('id_habitacion')
        ];
        if($habitacion->where($condicion)->delete() > 0){
            redirect('/admin/habitacion/listado/', 'habitación eliminada con éxito');
        }
    }
    public function elimininarImagen(Request $request){
        $image = new UploadFile();
        try{
            $image->setDeleteImage($request->_post('imagen'));
            $msj = 'si';
        }catch (\Exception $e){
            $msj = 'no';
        }
        echo json_encode(array(
            'message' => $msj
        ));
    }
    public function subirImagen(Request $request){
        $image = new UploadFile();
        try{
            $file = $request->_files('file');
            if(!empty($file)){
                $imagen = $image->setUploadImage($request->_files('file'));
                echo json_encode(array('image' => $imagen));
            }
        }catch (\Exception $e){
            die($e->getMessage());
        }
    }
    private function procesarReservacion(Request $request){
        $model = $this->Model('Reservaciones');
        /* busco el correo del cliente */
        $sql = "select correo, nombre, fecha_entrada, fecha_salida
                from adm_reservaciones a, adm_clientes b 
                where a.id_cliente = b.id_cliente and id_reservacion = ?";
        $rs_cliente = $model->raw($sql, array(intval($request->_post('pk'))));


        /* dependiendo que si la solicitud fue acepta o no se crea el template del correo electronico */
        switch ($request->_post('aceptada')){
            case 'Si':
                $asunto = "Su reserva ha sido aceptada";
                $body_mail = "
                    <h2>Hola, estimado(a): ".$rs_cliente['nombre']." </h2>
                ";

                /* actualiza la solicitud como procesada */
                $sql = "update adm_reservaciones set estado = 'P' where id_reservacion = ? ";
                break;
            case 'No':
                $asunto = "La solicitud de reserva ha sido rechazada";
                $body_mail = "
                    <h2>Hola, estimado(a): ".$rs_cliente['nombre']." </h2>
                    <p>La solicitud de reserva fue rechazada...</p>
                ";

                /* actualiza la solicitud como rechazada */
                $sql = "update adm_reservaciones set estado = 'R' where id_reservacion = ? ";
                break;
        }

        $mail = new SendMail();
        $destinatario =[
            $rs_cliente['correo'] => $rs_cliente['nombre']
        ];
        try{
            $mail->send($destinatario, $asunto, $body_mail);


            $model->raw($sql, array(intval($request->_post('pk'))));
            echo json_encode([
                'message' =>'bien'
            ]);
        }catch (\Exception $e){
            echo json_encode([
                'message' =>'mal'
            ]);
        }

    }
    public function listadoReservaciones(Request $request){
        if($request->isPost()){
            $this->procesarReservacion($request);
            return;
        }
        $model = $this->Model('Reservaciones');
        $sql = "select id_reservacion, nombre, telefono_movil, telefono_fijo, correo, nombre_habitacion,cantidad_adulto, cantidad_infante,
                       fecha_entrada, fecha_salida
                from adm_reservaciones a, adm_clientes b, adm_habitaciones c
                where a.id_cliente = b.id_cliente and a.id_habitacion = c.id_habitacion ";

        if($request->_get('tipo') == 'solicitud'){
            $sql .= " and a.estado = 'S' ";
            $titulo = 'Listado de reservaciones solicitadas ';
        }else{
            $sql .= " and a.estado = 'P' ";
            $titulo = 'Listado de reservaciones procesadas ';
        }


        $rs = $model->raw($sql);

        $context = [
            'reservaciones' => $rs,
            'titulo' => $titulo
        ];
        $this->render('reserva_listado', $context);
    }


}