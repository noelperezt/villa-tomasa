<?php

namespace apps\Admin\controllers;
use fw_Klipso\applications\login\Login;
use fw_Klipso\kernel\engine\middleware\Request;
use fw_Klipso\kernel\classes\abstracts\aController;

class SistemaController extends aController {
    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app){
        parent::__construct($app);
    }
    public function cerrarSesion(){
        $login = new Login();
        $login->makeLogout();
    }
    public function login(Request $request){
        if($request->isPost()){
            $login = new Login();
            $response = $login->makeLogin($request->_post('usuario'), $request->_post('clave'));

            if (is_bool($response) && $response == true){
                $redirect = DOMAIN_NAME . '/admin/dashboard';
                header("Location: $redirect");
            }else
                echo $response;
        }
        $context = [
            'titulo' => 'Iniciar sesión | Sitio de administración Villa tomasa'
        ];
        $this->render('login', $context);
    }

    public function dashboard(){
        $context = [
            'titulo' => 'Dashboard'
        ];
        $this->render('dashboard', $context);
    }
}