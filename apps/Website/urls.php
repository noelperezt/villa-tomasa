<?php
global  $route;

use fw_Klipso\kernel\engine\middleware\Session;



$route->add('','Website.WebsiteController.home');
$route->add('actividades/','Website.WebsiteController.actividades');
$route->add('habitaciones/','Website.WebsiteController.habitaciones');
$route->add('ubicacion/','Website.WebsiteController.ubicacion');
$route->add('galeria/','Website.WebsiteController.galeria');
$route->add('reservacion/','Website.WebsiteController.reservacion');
$route->add('reservacion/datos-habitacion/','Website.WebsiteController.getDatosHabitacion');