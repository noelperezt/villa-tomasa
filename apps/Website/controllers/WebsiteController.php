<?php
namespace apps\Website\controllers;

use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;
use fw_Klipso\kernel\engine\middleware\Response;


class WebsiteController extends aController{

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }
    public function home(){
        $this->render('home');
    }
    public function actividades(){
        $this->render('actividades');
    }
    public function galeria(){
        $this->render('galeria');
    }
    public function habitaciones(){
        $this->render('habitaciones');
    }
    public function ubicacion(){
        $this->render('ubicacion');
    }
    public function getDatosHabitacion(Request $request){
        $sql = "select descripcion_habitacion 
                from adm_habitaciones 
                where estado = 'A' and id_habitacion = ? ";

        try{
            $model = $this->Model('Habitaciones');
            $rs = $model->raw($sql, array($request->_post('habitacion')));

            $sql = "select ruta_archivo 
                    from adm_habitaciones_images 
                    where id_habitacion = ? ";

            $model = $this->Model('Habitaciones_imagenes');
            $rsImg = $model->raw($sql, array($request->_post('habitacion')));

            echo json_encode(array(
                'message' => 'bien',
                'descripcion_habitacion' => $rs,
                'imagenes' => $rsImg
            ));
        }catch (\PDOException $e){
            echo json_encode(array(
                'message' => $e->getMessage()
            ));
        }
    }
    private function guardarReservacion(Request $request){
        /* guardar el cliente */
        try{
            $this->DB()->setBeginTrans();
            $data = [
                'id_cliente' => intval(trim($request->_post('ci'))),
                'nombre' => $request->_post('nombre'),
                'telefono_movil'=> intval(trim($request->_post('telefono_movil'))),
                'telefono_fijo'=> intval(trim($request->_post('telefono_fijo'))),
                'correo'=> $request->_post('correo'),
            ];
            $this->DB()->qqInsert('adm_clientes', $data);

            $data = [
                'id_cliente' => intval(trim($request->_post('ci'))),
                'id_habitacion' => intval(trim($request->_post('habitacion'))),
                'cantidad_infante' => intval(trim($request->_post('cantidad_infante'))),
                'cantidad_adulto' => intval(trim($request->_post('cantidad_adulto'))),
                'fecha_entrada' => $request->_post('fecha_entrada'),
                'fecha_salida' => $request->_post('fecha_salida'),
            ];
            $this->DB()->qqInsert('adm_reservaciones', $data);
            $this->DB()->setCommit(true);
        }catch (\PDOException $e){
            $this->DB()->setCommit(false);
            Response::setMessage($e->getMessage());
        }



    }
    public function reservacion(Request $request){
        if($request->isPost())
            $this->guardarReservacion($request);

        $sql = "select id_habitacion, nombre_habitacion 
                from adm_habitaciones
                where estado = 'A' ";

        try{
            $model = $this->Model('Habitaciones');
            $rs = $model->raw($sql);
            $data  = [];
            if(!isset($rs[0]))
                $data[0] = $rs;
            else
                $data = $rs;
            $this->render('reservacion', array('habitaciones' => $data));
        }catch (\PDOException $e){
            Response::setMessage($e->getMessage());
            $this->render('reservacion');
        }

    }
}