<?php
namespace Hotel_Project;

use fw_Klipso\kernel\engine\middleware;

$route = new  middleware\Urls();


/*
    Write each of the url, example
    $route->add('','website.WebsiteController.home');

    Website is the application name. WebsiteController is the filename php and the class name and home
    is the method that is invoked when the url is called from the browser

    In this example, the home of website is in the WebsiteController

    para que alguna parte de la url sea enviada como parametro al controlador debe ser con el siguiente formato de
        ejemplo P{([0-9]+)}
*/

middleware\Request::excepCSRFTOKEN('admin/habitacion/subir-imagen');
middleware\Request::excepCSRFTOKEN('admin/habitacion/eliminar-imagen');
middleware\Request::excepCSRFTOKEN('reservacion/datos-habitacion');
middleware\Request::excepCSRFTOKEN('admin/reservaciones');



include BASE_DIR . 'apps/Admin/urls.php';

include BASE_DIR . 'apps/Website/urls.php';

$route->submit();
