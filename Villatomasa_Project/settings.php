<?php
namespace Villatomasa_Project;

/* domain name the application web */
define('DOMAIN_NAME', 'http://127.0.0.1:8000');

/* define your directory main */
define('BASE_DIR', __DIR__.'/../');

/* the folder folder for file static */
define('STATICFILES_DIRS', 'static');

/* extension file for templates */
define('EXT_TEMPLATE','twig');

/* urls for session start */
define('LOGIN_URL', '/admin/');
define('LOGOUT_REDIRECT_URL', '/admin/');

define('CLUSTER_UPLOAD_IMAGES', 'cluster/images');

define('SECRET_KEY', '56d6d15b9c0034a3f2a3d22731a83610515930aaa0b0e88b0c6f4c1a2fcb4b4e');

/* Lifetime in seconds of cookie */
define('SESSION_COOKIE_AGE', 60 * 40);

/* applications install */
define('APP_INSTALL',serialize([
    'fw_Klipso.applications.login',
    'apps.Admin'
]));

/* parameters for connecting the database */
define('DATABASE', serialize(array(
    'ENGINE' => 'mysql',
    'HOST' => 'localhost',
    'PORT' => '3306',
    'NAME' => 'tomasa_db',
    'PASSWORD' => 'mysql',
    'USER' => 'root'

)));

/* the name for ntemplate */
define('TEMPLATE_DIR','templates');

define('CLUSTER_UPLOAD_FILES', 'cluster');

/* Model that is used to register the users of the system and can log in */
define('USER_MODEL', 'User');

/* configuracion para usar la aplicacion de correo electronico */
define('SERVER_SMTP','a2plcpnl0416.prod.iad2.secureserver.net');
define('PORT_SMTP', 465);
define('TYPE_SECURE_SMTP','ssl');
define('USER_SMTP_FROM_NAME', 'noreply');
define('USER_SMTP', 'abejarano@pinttosoft.com');
define('PASS_SMTP', '130202@ngel');


if(isset($_SERVER['REQUEST_URI'])){
    /* include framework */
    require BASE_DIR.'fw_Klipso/kernel/engine.php';

    require 'urls.php';
}
